from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Receipt, ExpenseCategory, Account
from accounts.forms import ReceiptForm, AccountForm
from django.db import models
from expenses.accounts.forms import ExpenseCategoryForm


@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    return render(
        request, "receipts/receipt_list.html", {"receipts": receipts}
    )


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    return render(request, "receipts/create_receipt.html", {"form": form})


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(user_property=request.user)

    categories_with_counts = categories.annotate(
        num_receipts=models.Count("receipts")
    )

    return render(
        request,
        "receipts/category_list.html",
        {"categories": categories_with_counts},
    )


def account_list(request):
    accounts = Account.objects.filter(user_property=request.user)

    accounts_with_counts = accounts.annotate(
        num_receipts=models.Count("receipts")
    )
    return render(
        request,
        "receipts/account_list.html",
        {"accounts": accounts_with_counts},
    )


def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    return render(request, "receipts/create_category.html", {"form": form})


def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST, user=request.user)
        if form.is_valid():
            form.save()
            return redirect("home")
    else:
        form = AccountForm(user=request.user)
    return render(request, "create_account.html", {"form": form})
