from django.shortcuts import render, redirect

from django.contrib.auth import authenticate, login, logout
from django.urls import reverse
from .forms import LoginForm, SignUpForm
from django.contrib.auth.models import User


def login_view(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
            reverse("accounts:login")
    else:
        form = LoginForm()
    return render(request, "accounts/login.html", {"form": form})


def logout_view(request):
    logout(request)
    return redirect(reverse("accounts:login"))


def signup_view(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password == password_confirmation:
                user = User.objects.create_user(
                    username=username, password=password
                )
                login(request, user)
                return redirect("receipts:receipt_list")
            else:
                error_message = "The passwords do not match"
    else:
        form = SignUpForm()
        error_message = None
    return render(
        request,
        "accounts/signup.html",
        {"form": form, "error_message": error_message},
    )
